function TaxFromBrackets(taxableAmt, taxRates, taxCeilings) {
  var tax = 0.0;
  var floor = 0.0;
  for (i in taxRates) {
    tax += taxRates[i] * Math.max(0.0, Math.min(taxableAmt, taxCeilings[i]) - floor);
    floor = taxCeilings[i];
  }
  return tax;
}

// Filing Status
var SINGLE = 1;
var MFJ = 2;
var HOH = 4;

var INFINITY = 9999999.00;

// Good summary of state taxes here: https://taxfoundation.org/publications/state-individual-income-tax-rates-and-brackets/

function CaTax(year, filingStatus, income) {  // California
  const taxRates = [ 0.01, 0.02, 0.04, 0.06, 0.08, 0.093, 0.103, 0.113, 0.123, 0.133 ];
  const t = (year >= 2024) ? (
    (filingStatus == SINGLE || filingStatus == HOH) ? {
      stdDeduction: 5363.00,
      ceilings: [ 10412.00, 24684.00, 38959.00, 54081.00, 68350.00, 349137.00, 418961.00, 698271.00, 1000000.00, INFINITY ],
    } : (filingStatus == MFJ) ? {
      stdDeduction: 10726.00,
      ceilings: [ 20824.00, 49368.00, 77918.00, 108162.00, 136700.00, 698274.00, 837922.00, 1000000.00, 1396542.00, INFINITY ],
    } : {}
  ) : (year >= 2023) ? (
    (filingStatus == SINGLE || filingStatus == HOH) ? {
      stdDeduction: 5202.00,
      ceilings: [ 10099.00, 23942.00, 37788.00, 52455.00, 66295.00, 338639.00, 406364.00, 677275.00, 1000000.00, INFINITY ],
    } : (filingStatus == MFJ) ? {
      stdDeduction: 10404.00,
      ceilings: [ 20198.00, 47884.00, 75576.00, 104910.00, 132590.00, 677278.00, 812728.00, 1000000.00, 1354550.00, INFINITY ],
    } : {}
  ) : (year >= 2021) ? (
    (filingStatus == SINGLE || filingStatus == HOH) ? {
      stdDeduction: 4803.00,
      ceilings: [ 9325.00, 22107.00, 34892.00, 48435.00, 61214.00, 312686.00, 375221.00, 625369.00, 1000000.00, INFINITY ],
    } : (filingStatus == MFJ) ? {
      stdDeduction: 9606.00,
      ceilings: [ 18650.00, 44214.00, 69784.00, 96870.00, 122428.00, 625372.00, 750442.00, 1000000.00, 1250738.00, INFINITY ],
    } : {}
  ) : {};
  return TaxFromBrackets(income - t.stdDeduction, taxRates, t.ceilings);
}

function CoTax(year, filingStatus, income) {  // Colorado
  const taxRates = [ 0.044 ];
  const ceilings = [ INFINITY ];
  const t = (year >= 2024) ? (
    (filingStatus == SINGLE || filingStatus == HOH) ? {
      stdDeduction: 14600.00,
    } : (filingStatus == MFJ) ? {
      stdDeduction: 29200.00,
    } : {}
  ) : (year >= 2023) ? (
    (filingStatus == SINGLE || filingStatus == HOH) ? {
      stdDeduction: 13850.00,
    } : (filingStatus == MFJ) ? {
      stdDeduction: 27700.00,
    } : {}
  ) : {};
  return TaxFromBrackets(income - t.stdDeduction, taxRates, ceilings);
}

function DcTax(year, filingStatus, income) {  // Washington DC
  const taxRates = [ 0.04, 0.06, 0.065, 0.085, 0.0925, 0.0975, 0.1075 ];
  const ceilings = [ 10000.00, 40000.00, 60000.00, 250000.00, 500000.00, 1000000.00, INFINITY ];
  const t = (year >= 2024) ? (
    (filingStatus == SINGLE || filingStatus == HOH) ? {
      stdDeduction: 14600.00,
    } : (filingStatus == MFJ) ? {
      stdDeduction: 29200.00,
    } : {}
  ) : (year >= 2023) ? (
    (filingStatus == SINGLE || filingStatus == HOH) ? {
      stdDeduction: 13850.00,
    } : (filingStatus == MFJ) ? {
      stdDeduction: 27700.00,
    } : {}
  ) : (year < 2023) ? (
    (filingStatus == SINGLE || filingStatus == HOH) ? {
      stdDeduction: 12950.00,
    } : (filingStatus == MFJ) ? {
      stdDeduction: 25900.00,
    } : {}
  ) : {};
  return TaxFromBrackets(income - t.stdDeduction, taxRates, ceilings);
}

function IlTax(year, filingStatus, income) {  // Illinois
  const taxRate = 0.0495;
  const stdDeduction = 0.00;
  return TaxFromBrackets(income - stdDeduction, [taxRate], [INFINITY]);
}

function InTax(year, filingStatus, income) {  // Indiana
  const taxRate = 0.0305;
  const stdDeduction = 0.00;
  return TaxFromBrackets(income - stdDeduction, [taxRate], [INFINITY]);
}

function MdTax(year, filingStatus, income) {  // Maryland
  const taxRates = [ 0.02, 0.03, 0.04, 0.0475, 0.05, 0.0525, 0.055, 0.0575 ];
  const ceilings =
      (filingStatus == SINGLE || filingStatus == HOH)
          ? [ 1000.00, 2000.00, 3000.00, 100000.00, 125000.00, 150000.00, 250000.00, INFINITY ] :
      (filingStatus == MFJ)
          ? [ 1000.00, 2000.00, 3000.00, 150000.00, 175000.00, 225000.00, 300000.00, INFINITY ] : [];
  const t = (year >= 2024) ? (
    (filingStatus == SINGLE || filingStatus == HOH) ? {
      stdDeduction: 2550.00,
    } : (filingStatus == MFJ) ? {
      stdDeduction: 5150.00,
    } : {}
  ) : (year < 2024) ? (
    (filingStatus == SINGLE || filingStatus == HOH) ? {
      stdDeduction: 2400.00,
    } : (filingStatus == MFJ) ? {
      stdDeduction: 4850.00,
    } : {}
  ) : {};
  return TaxFromBrackets(income - t.stdDeduction, taxRates, ceilings);
}

function NcTax(year, filingStatus, income) {  // North Carolina
  const taxRate = (year >= 2024) ? 0.0450 : 0.0475;
  const t = (year >= 2021) ? (
    (filingStatus == SINGLE || filingStatus == HOH) ? {
      stdDeduction: 12750.00,
    } : (filingStatus == MFJ) ? {
      stdDeduction: 25500.00,
    } : {}
  ) : {};
  return TaxFromBrackets(income - t.stdDeduction, [taxRate], [INFINITY]);
}

function VaTax(year, filingStatus, income) {  // Virginia
  const taxRates = [ 0.02, 0.03, 0.05, 0.0575 ];
  const ceilings = [ 3000.00, 5000.00, 17000.00, INFINITY ];
  const stdDeduction =
      (filingStatus == SINGLE || filingStatus == HOH) ? 8000.00 :
      (filingStatus == MFJ) ? 16000.00 : 0/0;
  return TaxFromBrackets(income - stdDeduction, taxRates, ceilings);
}

function TaxableSocialSecurityBenefit(year, filingStatus, preSocialSecurityAGI, taxFreeInterest, socialSecurityBenefit) {
  const combinedIncome = preSocialSecurityAGI + taxFreeInterest + 0.5 * socialSecurityBenefit;
  const ssTaxRates = [ 0.00, 0.50, 0.85 ];
  const ssCeilings = (filingStatus == MFJ) ?
      [ 32000.00, 44000.00, INFINITY ] :
      [ 25000.00, 34000.00, INFINITY ];
  return Math.min(0.85 * socialSecurityBenefit, TaxFromBrackets(combinedIncome, ssTaxRates, ssCeilings));
}


function FedTaxInfo(year, filingStatus, tcjaSunset) {
  const t = (year >= 2025) ? (
    // https://taxfoundation.org/data/all/federal/2025-tax-brackets/
    (filingStatus == SINGLE) ? {
      stdDeduction: tcjaSunset ? 7850.00 : 15000.00,
      seniorDeduction: 2000,
      fedCeilings: [ 11925.00, 48475.00, 103350.00, 197300.00, 250525.00, 626350.00, INFINITY ],
      ltcgCeilings: [ 48350.00, 533400.00, INFINITY ],
      amtExemption: 88100.00,
      amtPhaseoutFloor: 626350.00,
      amtCeiling: 239100.00,
    } : (filingStatus == MFJ) ? {
      stdDeduction: tcjaSunset ? 15750.00 : 30000.00,
      seniorDeduction: 1600,
      fedCeilings: [ 23850.00, 96950.00, 206700.00, 394600.00, 501050.00, 751600.00, INFINITY ],
      ltcgCeilings: [ 96700.00, 600050.00, INFINITY ],
      amtExemption: 137000.00,
      amtPhaseoutFloor: 1252700.00,
      amtCeiling: 239100.00,
    } : (filingStatus == HOH) ? {
      stdDeduction: tcjaSunset ? 9350.00 : 22500.00,
      seniorDeduction: 2000,
      fedCeilings: [ 17000.00, 64850.00, 103350.00, 197300.00, 250050.00, 626350.00, INFINITY ],
      ltcgCeilings: [ 64750.00, 566700.00, INFINITY ],
      amtExemption: 88100.00,
      amtPhaseoutFloor: 626350.00,
      amtCeiling: 239100.00,
    } : {}
  ) : (year == 2024) ? (
    (filingStatus == SINGLE) ? {
      stdDeduction: tcjaSunset ? 7850.00 : 14600.00,
      seniorDeduction: 1950,
      fedCeilings: //tcjaSunset ? [ 11600.00, 47150.00, 113950.00, 237650.00, 516750.00, 518850.00, INFINITY ] :
        [ 11600.00, 47150.00, 100525.00, 191950.00, 243725.00, 609350.00, INFINITY ],
      ltcgCeilings: [ 47025.00, 518900.00, INFINITY ],
      amtExemption: 85700.00,
      amtPhaseoutFloor: 609350.00,
      amtCeiling: 232600.00,
    } : (filingStatus == MFJ) ? {
      stdDeduction: tcjaSunset ? 15750.00 : 29200.00,
      seniorDeduction: 1550,
      fedCeilings: //tcjaSunset ? [ 23200.00, 94300.00, 189850.00, 289250.00, 516750.00, 583750.00, INFINITY ] :
        [ 23200.00, 94300.00, 201050.00, 383900.00, 487450.00, 731200.00, INFINITY ],
      ltcgCeilings: [ 94050.00, 583750.00, INFINITY ],
      amtExemption: 133300.00,
      amtPhaseoutFloor: 1218700.00,
      amtCeiling: 232600.00,
    } : (filingStatus == HOH) ? {
      stdDeduction: tcjaSunset ? 9350.00 : 21900.00,
      seniorDeduction: 1950,
      fedCeilings: [ 16550.00, 63100.00, 100500.00, 191950.00, 243700.00, 609350.00, INFINITY ],
      ltcgCeilings: [ 63000.00, 551350.00, INFINITY ],
      amtExemption: 85700.00,
      amtPhaseoutFloor: 609350.00,
      amtCeiling: 232600.00,
    } : {}
  ) : (year == 2023) ? (
    (filingStatus == SINGLE) ? {
      stdDeduction: 13850.00,
      fedCeilings: [ 11000.00, 44725.00, 95375.00, 182100.00, 231250.00, 578125.00, INFINITY ],
      ltcgCeilings: [ 44625.00, 492300.00, INFINITY ],
      amtExemption: 81300.00,
      amtPhaseoutFloor: 578150.00,
      amtCeiling: 220700.00,
    } : (filingStatus == MFJ) ? {
      stdDeduction: 27700.00,
      fedCeilings: [ 22000.00, 89450.00, 190750.00, 364200.00, 462500.00, 693750.00, INFINITY ],
      ltcgCeilings: [ 89250.00, 553850.00, INFINITY ],
      amtExemption: 126500.00,
      amtPhaseoutFloor: 1156300.00,
      amtCeiling: 220700.00,
    } : (filingStatus == HOH) ? {
      stdDeduction: 20800.00,
      fedCeilings: [ 15700.00, 59850.00, 95350.00, 182100.00, 231250.00, 578100.00, INFINITY ],
      ltcgCeilings: [ 59750.00, 523050.00, INFINITY ],
      amtExemption: 81300.00,
      amtPhaseoutFloor: 578150.00,
      amtCeiling: 220700.00,
    } : {}
  ) : (year == 2022) ? (
    (filingStatus == SINGLE) ? {
      stdDeduction: 12950.00,
      fedCeilings: [ 10275.00, 41775.00, 89075.00, 170050.00, 215950.00, 539900.00, INFINITY ],
      ltcgCeilings: [ 41675.00, 459750.00, INFINITY ],
      amtExemption: 75900.00,
      amtPhaseoutFloor: 539900.00,
      amtCeiling: 206100.00,
    } : (filingStatus == MFJ) ? {
      stdDeduction: 25900.00,
      fedCeilings: [ 20550.00, 83550.00, 178150.00, 340100.00, 431900.00, 647850.00, INFINITY ],
      ltcgCeilings: [ 83350.00, 517200.00, INFINITY ],
      amtExemption: 118100.00,
      amtPhaseoutFloor: 1079800.00,
      amtCeiling: 206100.00,
    } : (filingStatus == HOH) ? {
      stdDeduction: 19400.00,
      fedCeilings: [ 14650.00, 55900.00, 89050.00, 170050.00, 215950.00, 539900.00, INFINITY ],
      ltcgCeilings: [ 55800.00, 488500.00, INFINITY ],
      amtExemption: 75900.00,
      amtPhaseoutFloor: 539900.00,
      amtCeiling: 206100.00,
    } : {}
  ) : {};
  return t;
}

// nonInvestment include qualified pensions, qualified annuities, Trad IRA withdrawals, 401k withdrawals, RMDs, and Roth conversions
// nonQualInvestment includes STCG, interest, and non-qualified dividends.
// qualInvestment include LTCG and qualified dividends.
// deductions includes QBI deduction plus either the standard deduction or the itemized deduction.
function FedTax(birthYear, year, filingStatus, tcjaSunset, earned, nonInvestment, nonQualInvestment, qualInvestment, taxFreeInterest, socialSecurityBenefit, adjustments, itemizedDeductions) {
  // The TCJA sunset may or may not occur in 2026.  https://en.wikipedia.org/wiki/Tax_Cuts_and_Jobs_Act
  const fedTaxRates = tcjaSunset
      ? [ 0.10, 0.15, 0.25, 0.28, 0.33, 0.35, 0.396 ]
      : [ 0.10, 0.12, 0.22, 0.24, 0.32, 0.35, 0.37 ];
  const ltcgTaxRates = [ 0.0, 0.15, 0.20 ];
  const niitRate = 0.038;
  const amtRates = [ 0.26, 0.28 ];
  const amtPhaseoutRate = 0.25;
  const niitDeduction = (filingStatus == SINGLE) ? 200000.00 : 250000.00;  // Not inflation indexed. :(
  const t = FedTaxInfo(year, filingStatus, tcjaSunset);
  const seniorDeduction = ((year - birthYear) >= 65) ? t.seniorDeduction : 0;
  const stdDeduction = t.stdDeduction + seniorDeduction;
  const deduction = Math.max(stdDeduction, itemizedDeductions);
  const investmentIncome = nonQualInvestment + qualInvestment;
  var agi = earned + nonInvestment + investmentIncome + adjustments;
  agi += TaxableSocialSecurityBenefit(year, filingStatus, agi, taxFreeInterest, socialSecurityBenefit);
  const fedTax = OrdinaryPlusLtcgTax(agi - deduction, qualInvestment, fedTaxRates, t.fedCeilings, ltcgTaxRates, t.ltcgCeilings);
  const amtExemption = Math.max(0, t.amtExemption - amtPhaseoutRate * Math.max(0, agi - t.amtPhaseoutFloor));
  const amtCeilings = [ t.amtCeiling, INFINITY ];
  const amt = OrdinaryPlusLtcgTax(agi - amtExemption, qualInvestment, amtRates, amtCeilings, ltcgTaxRates, t.ltcgCeilings);
  const niit = niitRate * Math.min(investmentIncome, Math.max(0, agi - niitDeduction));
  return niit + Math.max(amt, fedTax);
}

function OrdinaryPlusLtcgTax(taxable, ltcg, ordTaxRates, ordCeilings, ltcgTaxRates, ltcgCeilings) {
  const ordTax = TaxFromBrackets(taxable - ltcg, ordTaxRates, ordCeilings);
  const ltcgTax = TaxFromBrackets(taxable, ltcgTaxRates, ltcgCeilings) - TaxFromBrackets(taxable - ltcg, ltcgTaxRates, ltcgCeilings);
  return ordTax + ltcgTax;
}

function MedicareTax(year, filingStatus, earned) {
  const medicareTaxRates = [ 0.0145, 0.0235 ] ;
  const medicareCeiling = (filingStatus == MFJ) ? 250000.00 : 200000.00;  // Not inflation adjusted. :(
  const medicareCeilings = [ medicareCeiling, INFINITY ];
  return TaxFromBrackets(earned, medicareTaxRates, medicareCeilings);
}

function SocialSecurityTax(year, earned) {
  const socialSecurityTaxRates = [ 0.062, 0.0 ] ;
  const socialSecurityCeiling =
      (year == 2019) ? 132900.00 :
      (year == 2020) ? 137700.00 :
      (year == 2021) ? 142800.00 :
      (year == 2022) ? 147000.00 :
      (year == 2023) ? 160200.00 :
      (year == 2024) ? 168600.00 :
      (year == 2025) ? 176100.00 :
                       168600.00;
  const socialSecurityCeilings = [ socialSecurityCeiling, INFINITY ];
  return TaxFromBrackets(earned, socialSecurityTaxRates, socialSecurityCeilings);
}

function CalWithholding(year, filingStatus, earned) {
  // From worksheet on California form DE4.
  // https://edd.ca.gov/siteassets/files/pdf_pub_ctr/de4.pdf
  const withholdingRates = [ 0.011, 0.022, 0.044, 0.066, 0.088, 0.1023, 0.1133, 0.1243, 0.1353, 0.1463 ];
  const t = (filingStatus == SINGLE) ? {
    exemption: 5202.00,
    ceilings: [ 10099.00, 23942.00, 37788.00, 52455.00, 66295.00, 338639.00, 406364.00, 677275.00, 1000000.00, INFINITY ],
  } : (filingStatus == MFJ) ? {
    exemption: 10404.00,
    ceilings: [ 20198.00, 47884.00, 75576.00, 104910.00, 132590.00, 677278.00, 812728.00, 1000000.00, 1354550.00, INFINITY ],
  } : {};
  return TaxFromBrackets(earned - t.exemption, withholdingRates, t.ceilings);
}

function FederalWithholding(year, filingStatus, earned) {
  const withholdingRates = [ 0.00, 0.10, 0.12, 0.22, 0.24, 0.32, 0.35, 0.37 ];
  const t = (filingStatus == SINGLE) ? {
    exemption: 8650.00,
    ceilings: [ 4350.00, 14625.00, 46125.00, 93425.00, 174400.00, 220300.00, 544250.00, INFINITY ],
  } : (filingStatus == MFJ) ? {
    exemption: 12950.00,  // TODO: 12900.00 if only one spouse was working (one job).
    ceilings: [ 13000.00, 33550.00, 96550.00, 191150.00, 353100.00, 444900.00, 660850.00, INFINITY ],
  } : {};
  return TaxFromBrackets(earned - t.exemption, withholdingRates, t.ceilings);
}

function StateTax(year, filingStatus, state, income) {
  const stateTax =
      (state == "CA") ? CaTax(year, filingStatus, income) :
      (state == "CO") ? CoTax(year, filingStatus, income) :
      (state == "DC") ? DcTax(year, filingStatus, income) :
      (state == "MD") ? MdTax(year, filingStatus, income) :
      (state == "NC") ? NcTax(year, filingStatus, income) :
      (state == "VA") ? VaTax(year, filingStatus, income) :
      (state == "AK" || state == "FL" || state == "NV" || state == "SD" || state == "TN" || state == "TX" || state == "WA" || state == "WY") ? 0.0 :
      0/0;
  return stateTax;
}

// Returns the income tax (Federal, NIIT, AMT, FICA, State) for certain scenarios.
//
// Parameters:
//   birthYear = year of birth (need to determine if senior deduction applies)
//   year = 2023 | 2024
//   filingStatus: 1 (single) | 2 (married filing jointly) | 4 (head of household)
//   state = AK | CA | CO | DC | FL | NC | MD | NV | SD | TN | TX | VA | WA | WY
//   earned = earned income
//   nonInvestment = non-investment non-wage income (pension, rent, taxable portion of Social Security income)
//   nonQual = non-qualified non-earned income (e.g., interest, non-qualified dividends, short-term cap gains)
//   qual = qualified non-earned income (e.g., long-term cap gains, qualified dividends)
//   itemizedDeductions = Federal itemized deductions
function Tax(birthYear, year, filingStatus, state, earned, nonInvestment, nonQualInvestment, qualInvestment, taxFreeInterest, socialSecurityBenefit, adjustments, stateReadjustments, itemizedDeductions) {
  var flags = state;
  state = flags.substring(0, 2);
  flags = flags.substring(2);
  const tcjaSunsetRegex = /TCJA_SUNSET/g;
  const tcjaSunset = flags.match(tcjaSunsetRegex) ? 1 : 0;
  const fedTax = FedTax(birthYear, year, filingStatus, tcjaSunset, earned, nonInvestment, nonQualInvestment, qualInvestment, taxFreeInterest, socialSecurityBenefit, adjustments, itemizedDeductions);
  const ficaTax = MedicareTax(year, filingStatus, earned) + SocialSecurityTax(year, earned);
  return fedTax + ficaTax + StateTax(year, filingStatus, state, earned + nonInvestment + nonQualInvestment + qualInvestment + adjustments + stateReadjustments);
}

// Returns the before-tax withdrawal needed to fund a given after-tax spend using non-earned income (e.g., dividends and cap gains).
//
// Parameters:
//   year = 2023 | 2024
//   filingStatus: 1 (single) | 2 (married filing jointly) | 4 (head of household)
//   state = AK | CA | CO | DC | FL | NC | MD | NV | SD | TN | TX | VA | WA | WY
//   afterTaxGoal = desired spend, excluding taxes
//   nonQual = non-qualified non-earned income (e.g., interest, non-qualified dividends, short-term cap gains, taxable portion of Social Security income)
function TaxAdjustedGoal(year, filingStatus, state, afterTaxGoal, nonInvestment, nonQual) {
  const adjustments = 0;
  const stateReadjustments = 0;
  const itemizedDeductions = 0;
  const earned = 0;
  const taxFreeInterest = 0;
  const sococialSecurityBenefit = 0;
  const maxTaxRate = 0.75;
  var min = afterTaxGoal;
  var max = afterTaxGoal / (1 - maxTaxRate) + 1;
  const eps = 0.004;  // 4/10th of a cent.
  while (max - min > eps) {
    var x = (min + max) / 2;
    var tax = Tax(birthYear, year, filingStatus, state, earned, nonInvestment, nonQual, x - nonQual, taxFreeInterest, sococialSecurityBenefit, adjustments, stateReadjustments, itemizedDeductions);
    if (x - tax < afterTaxGoal) {
      min = x;
    } else {
      max = x;
    }
  }
  return (min + max) / 2;
}
